# KoronaBot

# What's this

Polls website, does md5 check for changed content and checks with regex for stuff, sends email conditionally.

# Installation

Create folder `/var/koronabot` (or customize path with `.env`) with RWX perms for user running this. Copy `.env.example` to `.env` and replace values there.

```shell
sudo mkdir /var/koronabot && sudo chown $USER: /var/koronabot && yarn install
```

# Running

```shell
yarn run exec
```

Or change values in example systemd service file `koronabot.service.example` and install to `/etc/systemd/system/koronabot.service`
