const dotenv = require('dotenv');
dotenv.config()
const fetch = require('node-fetch');
const { JSDOM } = require('jsdom');
const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const md5 = require('js-md5');

const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT || 587,
    secure: false,
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
    },
});

const informError = async (transporter, error) => {
    const to = process.env.MAINTAINER_MAIL;
    const from = process.env.MAIL_SENDER;
    const message = {
        from,
        to,
        subject: "Problem with koronabot",
        text: "Something's up with 'ronabot\n\n" +
        `\n${error}\n\n\n`+
        "Br, KoronaBot",
    }
    return await transporter.sendMail(message);
}

const sendEmail = async (transporter, foundOnPage) => {
    const to = process.env.NODE_ENV === 'production' ? process.env.MAIL_RECEPIENT : process.env.MAINTAINER_MAIL;
    const from = process.env.MAIL_SENDER;
    const message = {
        from,
        to,
        subject: "Koronarokotteen uutisia",
        text: "Koronarokotetta saattaisi olla saatavilla nyt:\n\n" +
        `\n${foundOnPage}\n\n\n`+
        "Tarkista https://www.raasepori.fi/etusivu/korona/koronarokotukset/\n\n" +
        "Terv, KoronaBot",
    }
    return await transporter.sendMail(message);
}

const saveMd5 = (path, md5Value) => fs.writeFileSync(path, md5Value);

const main = async () => {
    const url = `https://www.raasepori.fi/etusivu/korona/koronarokotukset?${(new Date().getTime())}`;
    const md5PersistPath = path.join(process.env.DATA_DIR, 'md5_persist.txt');

    const req = await fetch(url);

    const html = await req.text();

    const dom = new JSDOM(html);

    let mainContentText;
    try {
        mainContentText = dom.window.document.querySelector('main.content').textContent.trim();
    } catch (err) {
        informError(transporter, err);
        process.exit(1);
    }

    if(mainContentText.length < 1) { // We empty
        informError(transporter, "Main content text seems to be empty")
        process.exit(1);
    }

    const mainContentTextMd5 = md5(mainContentText);

    let lastMd5;
    try {
        lastMd5 = fs.readFileSync(md5PersistPath, "utf8");
    } catch(err) {
        if (err.code === 'ENOENT') {
            // File inexistent, save and set last md5 to be current
            saveMd5(md5PersistPath, mainContentTextMd5);
            lastMd5 = mainContentTextMd5;
        } else {
            throw err;
        }
    }

    if(mainContentTextMd5 === lastMd5) {
        // No changes, exit
        console.info("Main content text unchanged, exiting");
        process.exit(0);
    } else {
        saveMd5(md5PersistPath, mainContentTextMd5);
    }

    const selector = 'main ul li';

    const candidates = Array.from(dom.window.document.querySelectorAll(selector)).map(el => el.textContent.trim());

    const rgx = /.*19([5,6,7][0-9]|4[5-9]) syntyneet.*/g

    const matches = candidates.filter((textContent) => rgx.test(textContent));

    if(matches.length > 0) {
        console.log("Found match(es), sending email");
        console.log(matches);
        let mailResult;
        try {
            mailResult = await sendEmail(transporter, matches.join("\n\n"));
        } catch (err) {
            console.error("Error sending notification email");
            throw err;
        }
        console.log("Sent email to", mailResult.accepted.join(', '));
        process.exit(0);
    } else {
        console.log("No match, exiting");
        process.exit(0);
    }
}

main().then(() => {
    process.exit(0);
}).catch((e) => {
    console.error(e);
    informError(transporter, e.stack).then(() => {
        process.exit(1);
    }).catch((e) => {
        console.error(e);
    });
});
